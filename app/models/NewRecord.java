package models;

import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.security.ColumnVisibility;
import org.apache.hadoop.io.Text;
import play.data.validation.Constraints;

import java.util.UUID;

/**
 * Created by jeff on 3/12/14.
 */
public class NewRecord {
    @Constraints.Required
    public String table;

    public String newTableName;

    public String rowId;

    @Constraints.Required
    public String colFamily;

    @Constraints.Required
    public String colQualifier;

    public String visibility;

    @Constraints.Required
    public String value;

    public Mutation toMutation() {
        if (rowId == null || rowId.isEmpty()) {
            rowId = UUID.randomUUID().toString();
        }
        Text tRowId = new Text(rowId);
        Text tColFam = new Text(colFamily);
        Text tColQual = new Text(colQualifier);
        Value tValue = new Value(value.getBytes());
        ColumnVisibility tColVis = new ColumnVisibility(visibility);
        long timestamp = System.currentTimeMillis();

        Mutation mutation = new Mutation(tRowId);
        mutation.put(tColFam, tColQual, tColVis, timestamp, tValue);

        return mutation;

    }
}