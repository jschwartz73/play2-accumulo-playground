package helpers;

import com.schwartech.accumulo.ext.helpers.UserHelper;
import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.security.Authorizations;

/**
 * Created by jeff on 3/26/14.
 */
public class UserRoleHelper {
    public static String getRolesAsString(String username) throws AccumuloSecurityException, AccumuloException {
        Authorizations auths = UserHelper.getAuths(username);
        StringBuilder roles = new StringBuilder();
        for (byte[] auth : auths.getAuthorizations()) {
            roles.append(new String(auth));
            roles.append(", ");
        }

        return trimRoles(roles);
    }

    private static String trimRoles(StringBuilder roles) {
        if (roles.lastIndexOf(",") > 0) {
            roles.delete(roles.lastIndexOf(","), roles.lastIndexOf(",")+1);
        }
        return roles.toString().trim();
    }


}
