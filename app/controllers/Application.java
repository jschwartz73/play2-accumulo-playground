package controllers;

import com.schwartech.accumulo.Accumulo;
import com.schwartech.accumulo.ext.model.Document;
import com.schwartech.accumulo.ext.model.DocumentIndex;
import com.schwartech.accumulo.ext.model.DocumentIndexResultSet;
import com.schwartech.accumulo.ext.helpers.DataHelper;
import com.schwartech.accumulo.ext.helpers.TableHelper;
import com.schwartech.accumulo.ext.helpers.UserHelper;
import com.schwartech.accumulo.ext.model.DocumentResultSet;
import helpers.UserRoleHelper;
import models.*;
import org.apache.accumulo.core.cli.ScannerOpts;
import org.apache.accumulo.core.client.*;

import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.SkippingIterator;
import org.apache.accumulo.core.iterators.user.IntersectingIterator;
import org.apache.accumulo.core.iterators.user.WholeRowIterator;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.accumulo.core.security.ColumnVisibility;
import org.apache.hadoop.io.Text;
import play.*;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.*;

public class Application extends Controller {

    private static Form<TableView> formTableView = Form.form(TableView.class);
    private static Form<NewRecord> formNewRecord = Form.form(NewRecord.class);
    private static SecureRandom random = new SecureRandom();

    public static Result index() throws Exception {
        SortedSet<String> tables = TableHelper.getUserTables();
        Map<Key, Value> data = new HashMap<Key,Value>();
        return ok(index.render(TableHelper.getUserTables(), UserHelper.getUsernames(), formTableView, data, "", "", ""));
    }

    private static String getRandomText() {
        return new BigInteger(130, random).toString(32);
    }

    public static Result generateIndexedDummyData() throws Exception {
        String[] columnNames = {"first_name", "last_name", "mrn", "field4", "field5", "field6"};

        String tableNamePrefix = getRandomText();
        String tableNameData = tableNamePrefix + "Data";
        String tableNameIndex = tableNamePrefix + "Index";

        TableHelper.createTable(tableNameData);
        TableHelper.createTable(tableNameIndex);

        BatchWriter batchWriterData = Accumulo.createBatchWriter(tableNameData);
        BatchWriter batchWriterIndex = Accumulo.createBatchWriter(tableNameIndex);

        String colFamily = "form1";
        for (int row=0; row<25; row++) {
            String rowId = UUID.randomUUID().toString();

            Text tRowId = new Text(rowId);
            Text tColFam = new Text(colFamily);
            ColumnVisibility tColVis = new ColumnVisibility();
            long timestamp = System.currentTimeMillis();

            Mutation[] indexMutations = new Mutation[columnNames.length];
            Mutation dataMutation = new Mutation(tRowId);
            int idx = 0;
            for (String col : columnNames) {
                indexMutations[idx] = new Mutation(col + (row+1));
//                if (row < 3) {
                    indexMutations[idx].put(tColFam, tRowId, new ColumnVisibility(), new Value("".getBytes()));
//                } else if (row < 7) {
//                    indexMutations[idx].put(tColFam, tRowId, new ColumnVisibility("root"), new Value("".getBytes()));
//                } else {
//                    indexMutations[idx].put(tColFam, tRowId, new ColumnVisibility("jeff"), new Value("".getBytes()));
//                }

                Text tColQual = new Text(col);
                Value tValue = new Value(getRandomText().getBytes());
                dataMutation.put(tColFam, tColQual, tColVis, timestamp, tValue);
                Logger.info("GeneratedData: " + tColFam + "/" + tColQual + ":" + tValue);
                idx++;
            }
            batchWriterData.addMutation(dataMutation);

            batchWriterIndex.addMutations(Arrays.asList(indexMutations));
        }
        batchWriterData.close();
        batchWriterIndex.close();

        flash("success", "Table " + tableNameData + " created with 10 rows and 6 columns");
        return redirect(routes.Application.index());
    }

    public static Result viewTableLastRow(String table, String lastRow, String username) throws Exception {
        Map<Key, Value> map = new HashMap<Key,Value>();

        if (!TableHelper.tableExists(table)) {
            return redirect(routes.Application.index());
        }
        Authorizations auths = UserHelper.getAuths(username);

        Text lastRowText = new Text(lastRow);
        Logger.info("lastRow: " + lastRowText);

        ScannerOpts scanOpts = new ScannerOpts();
        Scanner scanner = Accumulo.createScanner(table, auths);
        scanner.setBatchSize(scanOpts.scanBatchSize);
        scanner.fetchColumnFamily(new Text("form1"));

        scanner.setRange(new Range(lastRowText, false, new Text("~~"), true));

        int i = 0;
        Iterator<Map.Entry<Key,Value>> iter = scanner.iterator();
        while (iter.hasNext() && i < 10) {
            Map.Entry<Key, Value> entry = iter.next();
            Logger.info("Found["+i+"]: " + entry.getKey() + "=" + entry.getValue() + " [NEXT] " + iter.hasNext());
            i++;
        }

        Form<TableView> data = formTableView.bindFromRequest();

        return ok(
            index.render(
                TableHelper.getUserTables(),
                UserHelper.getUsernames(),
                data,
                map,
                UserRoleHelper.getRolesAsString(username),
                table,
                ""
            )
        );
    }

    public static Result viewTable() throws Exception {
        Map<Key, Value> map = new HashMap<Key,Value>();

        Form<TableView> data = formTableView.bindFromRequest();
        String table = data.get().selectedTable;
        if (data.hasErrors()) {
            return badRequest(index.render(TableHelper.getUserTables(), UserHelper.getUsernames(), data, map, "", table, ""));
        }

        String username = data.get().username;
        if (username.isEmpty()) {
            username = Configuration.root().getString("accumulo.username", "root");
        }

        if (!TableHelper.tableExists(table)) {
            return redirect(routes.Application.index());
        }
        Authorizations auths = UserHelper.getAuths(username);

        ScannerOpts scanOpts = new ScannerOpts();
        Scanner scanner = Accumulo.createScanner(table, auths);
        scanner.setBatchSize(scanOpts.scanBatchSize);

        String tempRow = null;
        DocumentResultSet drs = new DocumentResultSet();
        for (Map.Entry<Key,Value> entry : scanner) {
            map.put(entry.getKey(), entry.getValue());
            drs.add(entry.getKey(), entry.getValue());
            tempRow = entry.getKey().getRow().toString();
//            Key key = entry.getKey();
//            Logger.info("Row: " + key.getRow() + ", colFam: " + key.getColumnFamily() + ", col: " + key.getColumnFamilyData() + ", qual: " + key.getColumnQualifier() + ", vis: " + key.getColumnVisibility());
//            System.out.println(entry.getKey().toString() + " -> " + entry.getValue().toString());
        }

        if (tempRow != null) {
            Document d = drs.get(tempRow);
            if (d == null) {
                Logger.info("Document not found: " + tempRow);
            } else {
                Value fname = d.getValue("form", "firstname");
                String lname = d.getValueAsString("form", "lastname");
                Logger.info("found document: " + fname + " " + lname);
            }
        }

        return ok(index.render(TableHelper.getUserTables(), UserHelper.getUsernames(), data, map, UserRoleHelper.getRolesAsString(username), table, ""));
    }

    public static Result getRecord(String username, String table, String rowId) throws Exception {
        DynamicForm dForm = new DynamicForm().bindFromRequest();
        String tUsername = dForm.get("username");
        if (tUsername != null) {
            username = tUsername;
        }

        Authorizations auths = UserHelper.getAuths(username);

        ScannerOpts scanOpts = new ScannerOpts();
        Scanner scanner = Accumulo.createScanner(table, auths);
        scanner.setBatchSize(scanOpts.scanBatchSize);
        scanner.setRange(new Range(rowId));

        Map<Key, Value> map = new HashMap<Key,Value>();
        for (Map.Entry<Key,Value> entry : scanner) {
            map.put(entry.getKey(), entry.getValue());
//            Key key = entry.getKey();
//            Logger.info("Row: " + key.getRow() + ", colFam: " + key.getColumnFamily() + ", col: " + key.getColumnFamilyData() + ", qual: " + key.getColumnQualifier() + ", vis: " + key.getColumnVisibility());
//            System.out.println(entry.getKey().toString() + " -> " + entry.getValue().toString());
        }

        return ok(viewRecord.render(UserHelper.getUsernames(), username, table, rowId, map));
    }

    public static Result createTable() throws Exception {
        DynamicForm dForm = new DynamicForm().bindFromRequest();
        String tableName = dForm.get("tableName");

        TableHelper.createTable(tableName);
        flash("success", "Success.  Table created: " + tableName);
        return redirect(routes.Application.index());
    }

    public static Result showCreateRecord() throws Exception {
        return ok(createRecord.render(TableHelper.getUserTables(), formNewRecord));
    }

    public static Result createRecord() throws Exception {
        Form<NewRecord> data = formNewRecord.bindFromRequest();
        if (data.hasErrors()) {
            return badRequest(createRecord.render(TableHelper.getUserTables(), data));
        }

        NewRecord record = data.get();
        String tableName = record.table;
        if ("<NEW TABLE>".equals(tableName)) {
            tableName = record.newTableName;
            TableHelper.createTable(tableName);
        }

        BatchWriter writer = Accumulo.createBatchWriter(tableName);

        Mutation m = record.toMutation();
        writer.addMutation(m);

        writer.close();

        return redirect(routes.Application.index());
    }

    public static Result getUsers() throws Exception {
        Map<String, String> userRoleMap = new HashMap<String,String>();
        Set<String> users = UserHelper.getUsernames();
        for (String user : users) {
            userRoleMap.put(user, UserRoleHelper.getRolesAsString(user));
        }
        return ok(userList.render(userRoleMap));
    }

    public static Result deleteUser(String username) throws Exception {
        UserHelper.deleteUser(username);
        flash("success", "Success.  Removed user: " + username);
        return redirect(routes.Application.getUsers());
    }
    public static Result getUser(String username) throws Exception {
        Set<byte[]> tRoles = UserHelper.getRolesAsSet(username);
        Set<String> roles = new HashSet<String>();
        for (byte[] role : tRoles) {
            roles.add(new String(role));
        }
        return ok(user.render(username, roles));
    }

    public static Result removeRole(String username, String role) throws Exception {
        UserHelper.removeRole(username, role);
        return redirect(routes.Application.getUser(username));
    }

    public static Result addRole(String username) throws Exception {
        DynamicForm dForm = new DynamicForm().bindFromRequest();
        String role = dForm.get("role");

        UserHelper.addRole(username, role);
        return redirect(routes.Application.getUser(username));
    }

    public static Result createUser() throws Exception {
        DynamicForm dForm = new DynamicForm().bindFromRequest();
        String username = dForm.get("username");
        String password = dForm.get("password");

        UserHelper.createUser(username, password);
        return redirect(routes.Application.getUsers());
    }

    public static Result authenticateUser() throws Exception {
        DynamicForm dForm = new DynamicForm().bindFromRequest();
        String username = dForm.get("username");
        String password = dForm.get("password");

        boolean success = UserHelper.validateUser(username, password);
        if (success) {
            flash("auth-success", "Authentication succeeded");
        } else {
            flash("auth-failure", "Authentication failed");
        }
        return redirect(routes.Application.getUsers());
    }

}