name := "AccumuloTest"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  cache,
  "com.schwartech" %% "accumulo-ext-plugin" % "1.0-SNAPSHOT"
)

resolvers += (
    "SchwarTech GitHub Repository" at "http://jschwartz73.github.com/m2repo/releases/"
)

play.Project.playJavaSettings
